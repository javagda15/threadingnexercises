package pl.sda.javagda15.exercises.zad1_phone;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        MessageBox messageBox = new MessageBox();

        Scanner scanner = new Scanner(System.in);
        String linia;

        do {
            linia = scanner.nextLine();

            if (linia.startsWith("dodaj")) {
                messageBox.addMessage(linia.split(" ", 2)[1]);
            }else if(linia.equalsIgnoreCase("print")){
                messageBox.printAllMessagesStack();
            }

        } while (!linia.equalsIgnoreCase("quit"));
    }
}
