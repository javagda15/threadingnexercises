package pl.sda.javagda15.exercises.zad1_phone;

import java.time.LocalDateTime;

public class Message {
    private String content;
    private LocalDateTime receiveTime;

    public Message(String content) {
        this.receiveTime = LocalDateTime.now();
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDateTime getReceiveTime() {
        return receiveTime;
    }

    @Override
    public String toString() {
        return "Message{" +
                "content='" + content + '\'' +
                ", receiveTime=" + receiveTime +
                '}';
    }
}
