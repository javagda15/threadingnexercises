package pl.sda.javagda15.exercises.zad1_phone;

import java.util.Stack;

public class MessageBox {
    private Stack<Message> messages = new Stack<>();

    public void addMessage(String content) {
        Message message = new Message(content);
        messages.push(message); // add - dodanie = push
    }

    public Message removeLast() {
        // usuwa i zwraca wiadomość
        return messages.pop();
    }

//    zachowa kolejność dodawania
    public void printAllMessages(){
        for (Message m: messages) {
            System.out.println(m);
        }
    }

    public void printAllMessagesStack(){
        if(messages.isEmpty()){
            return;
        }
        Message wiadomosc = messages.pop();

        System.out.println(wiadomosc);
        printAllMessagesStack();

        messages.push(wiadomosc);
    }
}
