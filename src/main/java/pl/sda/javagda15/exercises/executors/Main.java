package pl.sda.javagda15.exercises.executors;

import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    public static void main(String[] args) {
        // stworzenie puli wątków - polityka cached
        ExecutorService pulaWatkow = Executors.newCachedThreadPool();
        // stworzenie puli - stała pula wątków
//        ExecutorService pulaWatkow = Executors.newFixedThreadPool(3);
//
//        ExecutorService pulaWatkow = Executors.newScheduledThreadPool(2);
        // pojedynczy watek
//        ExecutorService pulaWatkow = Executors.newSingleThreadExecutor();

        Scanner scanner = new Scanner(System.in);
        String linia;
        do {
            linia = scanner.nextLine();

            final Long time = Long.parseLong(linia);
            pulaWatkow.submit(() -> {
                System.out.println("Start " + time);
                try {
                    Thread.sleep(time);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Timer " + time + " koniec");
            });

        } while (!linia.equalsIgnoreCase("quit"));

    }
}
