package pl.sda.javagda15.exercises.threading;

import java.io.File;
import java.util.Scanner;

public class Example {
    public static void main(String[] args) {
        Thread watek = new Thread(new Runnable() {
            @Override
            public void run() {
                System.err.println("Start wątku.");

//                try {
//                    Thread.sleep(5000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
                int i;
                for (i = 0; i < 1000000; i++) {
                    System.err.println("" + i);
                }
                for (; i > 0; --i) {
                    System.err.println("" + i);
                }

//                long lastLength = 0;
//                while (true) {
//                    File file = new File("wejscie");
//                    if (file.length() != lastLength) {
//                        System.out.println("Plik się zmienił");
//                        lastLength = file.length();
//                    }
//                    try {
//                        Thread.sleep(1);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }

                System.err.println("Koniec pracy wątku!");
            }
        });
        watek.start();
        try {
            watek.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Wznawiam działanie");

        Scanner scanner = new Scanner(System.in);
        String linia;
        do {
            linia = scanner.nextLine();

            System.out.println("Otrzymałem: " + linia);
        } while (!linia.equalsIgnoreCase("quit"));
    }
}
