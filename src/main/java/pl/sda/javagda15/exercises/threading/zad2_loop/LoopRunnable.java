package pl.sda.javagda15.exercises.threading.zad2_loop;

public class LoopRunnable implements Runnable {
    private String message;

    public LoopRunnable() {
    }

    @Override
    public void run() {
        String exclamationMarks = "";

        for (int i = 0; i < 100; i++) {
            try {
                // co 10 sekund
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // dodaje wykrzykniki
            exclamationMarks += "!";

            // wypisuje komunikat
            System.out.println(i + ". Hello World" + exclamationMarks + " - " + message);
        }
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
