package pl.sda.javagda15.exercises.threading.zad1_30sek;

public class MainZad1 {
    public static void main(String[] args) {
        Thread watek = new Thread(() -> {
            // wątek startuje
            try {
                // przesypia 30 sek
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // po zakończeniu 'drzemki' wypisuje komunikat
            System.out.println("Juhuuuu!");
        });

        // !!! Pamiętaj wystartować wątek
        watek.start();
    }
}
