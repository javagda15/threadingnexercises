package pl.sda.javagda15.exercises.threading.zad2_loop;

import java.util.Scanner;

public class MainZad2 {
    public static void main(String[] args) {
        LoopRunnable loopRunnable = new LoopRunnable();
        Thread watek = new Thread(loopRunnable);

        watek.start();

        Scanner scanner = new Scanner(System.in);
        String linia;
        do {
            linia = scanner.nextLine();

            loopRunnable.setMessage(linia);

        } while (!linia.equalsIgnoreCase("quit"));
        System.out.println("Huraaaa!");
    }
}
