package pl.sda.javagda15.exercises.threading.zad3_folderCheck;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

public class FolderChecker implements Runnable {
    private String sciezkaSprawdzana;
    private Set<File> fileSet = new HashSet<>();

    public FolderChecker() {
        this.sciezkaSprawdzana = ".";
    }

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            // folder sprawdzany
            File sprawdzany = new File(sciezkaSprawdzana);

            // tworze zbior plikow (obecnych)
            Set<File> fileNames = new HashSet<>();
            for (File f : sprawdzany.listFiles()) {
                fileNames.add(f);
            }

            // sprawdzam rozmiary zbiorów
            if (fileSet.size() != fileNames.size()) {
                System.out.println("Różna ilość plików");
            }

            Set<File> kopia = new HashSet<>(fileNames);

            // pozostawi mi w zbiorze fileNames tylko nowe pliki
            fileNames.removeAll(fileSet);

            // dodanie nowych plików do kolekcji fileSet
            for (File plik : fileNames) {
                System.out.println("Dodano nowy plik: " + plik.getName());
                fileSet.add(plik);
            }

            Set<File> kopiaFileSet = new HashSet<>(fileSet);
            kopiaFileSet.removeAll(kopia);

            // usunięcie usuniętych plików
            for (File plik : kopiaFileSet) {
                System.out.println("Usunięto plik: " + plik.getName());
                fileSet.remove(plik);
            }
        }
    }

    public void setSciezkaSprawdzana(String sciezkaSprawdzana) {
        this.sciezkaSprawdzana = sciezkaSprawdzana;
    }
}
