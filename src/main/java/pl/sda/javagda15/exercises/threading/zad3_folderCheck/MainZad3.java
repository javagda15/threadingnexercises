package pl.sda.javagda15.exercises.threading.zad3_folderCheck;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class MainZad3 {
    public static void main(String[] args) {
        FolderChecker folderChecker = new FolderChecker();

        Thread watek = new Thread(folderChecker);

        watek.start();

        Scanner scanner = new Scanner(System.in);
        String linia;
        do {
            linia = scanner.nextLine();

            folderChecker.setSciezkaSprawdzana(linia);
        } while (!linia.equalsIgnoreCase("quit"));
    }
}
