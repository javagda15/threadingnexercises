package pl.sda.javagda15.exercises.threading.zad5_timer;

import java.util.Scanner;

public class MainZad5 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String linia;
        do {
            linia = scanner.nextLine();

            final Long time = Long.parseLong(linia);
            Thread watek = new Thread(() -> {
                try {
                    Thread.sleep(time);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Timer " + time + " koniec");
            });
            watek.start();

        } while (!linia.equalsIgnoreCase("quit"));
    }
}
