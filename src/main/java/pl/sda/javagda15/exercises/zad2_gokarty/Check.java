package pl.sda.javagda15.exercises.zad2_gokarty;

import java.time.Duration;
import java.time.LocalDateTime;

public class Check {
    private Client client;
    private LocalDateTime arrivalTime;
    private Duration duration;
    private double amount;

    public Check(Client client, LocalDateTime arrivalTime) {
        this.client = client;
        this.arrivalTime = arrivalTime;
        this.duration = Duration.between(arrivalTime, LocalDateTime.now());
        this.amount = 20;

        long seconds = this.duration.getSeconds();
        long minutes = seconds / 60;
        long tenMinutes = minutes / 10;

        tenMinutes -= 6; //(ucinam pierwszą godzinę - zostają tylko pozostałe 10 minutówki)

        // po usunięciu 60 min zostaje mi tylko czas który jest ponad pierwszą godzinę i
        // mam go w postaci 10-minutówek. każda kosztuje 1 zł.
        if (tenMinutes > 0) {
            this.amount += tenMinutes;
        }

        int discount = client.getDiscountType().getDiscountPercentage();
        this.amount = (this.amount * (100 - discount)) / 100.0;
    }

    public Client getClient() {
        return client;
    }

    public LocalDateTime getArrivalTime() {
        return arrivalTime;
    }

    public Duration getDuration() {
        return duration;
    }

    public double getAmount() {
        return amount;
    }

    public double getAmountWithTax() {
        return amount * 1.08;
    }

    @Override
    public String toString() {
        return "Check{" +
                "client=" + client +
                ", arrivalTime=" + arrivalTime +
                ", duration=" + duration +
                ", amount=" + amount +
                '}';
    }
}
