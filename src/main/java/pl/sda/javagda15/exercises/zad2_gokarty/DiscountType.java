package pl.sda.javagda15.exercises.zad2_gokarty;

public enum DiscountType {
    NONE(0), SCHOOL(51), STUDENT(49), SENIOR(90);

    private final int discountPercentage;

    DiscountType(int discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public int getDiscountPercentage() {
        return discountPercentage;
    }
}
