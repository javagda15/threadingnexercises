package pl.sda.javagda15.exercises.zad2_gokarty;

public class Client {
    private String name, surname, pesel;
    private DiscountType discountType;

    public Client(String name, String surname, String pesel, DiscountType discountType) {
        this.name = name;
        this.surname = surname;
        this.pesel = pesel;
        this.discountType = discountType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public DiscountType getDiscountType() {
        return discountType;
    }

    public void setDiscountType(DiscountType discountType) {
        this.discountType = discountType;
    }

    @Override
    public String toString() {
        return "Client{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", pesel='" + pesel + '\'' +
                ", discountType=" + discountType +
                '}';
    }
}
