package pl.sda.javagda15.exercises.zad2_gokarty;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        GokartTrack track = new GokartTrack();
        String line;
        do {
            line = scanner.nextLine();

            String[] slowa = line.split(" ");
            if (slowa[0].equalsIgnoreCase("dodaj")) {

                Client c = new Client(slowa[1], slowa[2], slowa[3], DiscountType.valueOf(slowa[4]));
                track.addClient(c);
            } else if (slowa[0].equalsIgnoreCase("rachunek")) {
                track.printCheck(slowa[1]);
            } else if (slowa[0].equalsIgnoreCase("rachunki")) {
                track.printChecks();
            } else if (slowa[0].equalsIgnoreCase("ilosc")) {
                System.out.println(track.getClientCount());
            } else if (slowa[0].equalsIgnoreCase("seniorzy")) {
                System.out.println(track.getSeniorCount());
            }
        } while (!line.equalsIgnoreCase("quit"));
    }
}
