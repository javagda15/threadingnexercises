package pl.sda.javagda15.exercises.zad2_gokarty;

import java.time.LocalDateTime;
import java.util.*;

public class GokartTrack {
    private Map<Client, LocalDateTime> clientMap = new HashMap<>();
    private List<Check> givenChecks = new ArrayList<>();

    public void addClient(Client newClient) {
        clientMap.put(newClient, LocalDateTime.now());
    }

    public Optional<Check> printCheck(String pesel) {
        Optional<Map.Entry<Client, LocalDateTime>> client = clientMap.entrySet()
                .stream()
                .filter(entry -> entry.getKey().getPesel().equalsIgnoreCase(pesel)).findFirst();
        if (client.isPresent()) {
            Map.Entry<Client, LocalDateTime> mapEntry = client.get();
            Check check = new Check(mapEntry.getKey(), mapEntry.getValue());

            givenChecks.add(check); // dodanie do listy (zapamiętanie) rachunku
            clientMap.remove(mapEntry.getKey()); // usuwam z mapy żeby nie drukować temu
            // samemu klientowi kolejnego
            //  (takiego samego) rachunku

            return Optional.ofNullable(check);
        }

        //
        return Optional.empty();
    }

    public void printChecks() {
        for (Check check : givenChecks) {
            System.out.println(check);
        }
    }

    public int getClientCount() {
        return clientMap.size();
    }

    public long getSeniorCount() {
        return clientMap.keySet()
                .stream()
                .filter(k -> k.getDiscountType() == DiscountType.SENIOR)
                .count();
    }

}
